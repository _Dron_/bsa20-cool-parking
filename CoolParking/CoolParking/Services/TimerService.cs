﻿

 using System;
 using System.Timers;
 using CoolParking.Interfaces;
 using CoolParking.Models;

 namespace CoolParking.Services
 {
     class TimerService:ITimerService
     {
         public Vehicle _Car;
         public event ElapsedEventHandler Elapsed;
         private Timer _timer;
         public double Interval { get; set; }
         public void Start()
         {
             _timer.Start();
             _timer.Elapsed += elapsed;
         }

         public TimerService(int interval,Vehicle car)
         {
             _timer = new Timer(interval);
             _Car = car;
         }
         public void Stop()
         {
             _timer.Stop();
         }

         void ITimerService.Dispose()
         {
             ((ITimerService) this).Dispose();
         }

         void elapsed(object s, ElapsedEventArgs e)
         {
              Elapsed(this, e);
         }
     
     }
 }