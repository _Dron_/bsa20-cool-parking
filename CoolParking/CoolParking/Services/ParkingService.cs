﻿
 using System;
 using System.Collections.Generic;
 using System.Collections.ObjectModel;
 using System.Linq;
 using System.Security.Cryptography.X509Certificates;
 using System.Timers;
 using CoolParking.Interfaces;
 using CoolParking.Models;

 namespace CoolParking.Services
 {
     class ParkingService:IParkingService
     {
         private decimal _carBalance;
         private Collection<TimerService> _startedServices=new Collection<TimerService>();
         private Collection<LogService> _createdServices=new Collection<LogService>();


         public void Dispose()
         {
             Dispose();
         }

         public decimal GetBalance()
         {
             int i = 1;
             foreach (Vehicle v in Parking._vechiles)
             {
                 Console.WriteLine(i+")"+v._id+" "+v._type);
             }
             A:
             Console.WriteLine("Введiть цифру вашоi машини: ");
             try
             {
                 return Parking._vechiles[Convert.ToInt32(Console.ReadKey()) - 1]._balance;
             }
             catch (Exception ex)
             {
                 Console.WriteLine("Помилка , спробуйте знову");
                 goto A;
             }
             
         }

         public int GetCapacity()
         {
             return Settings.MaxSlots;
         }

         public int GetFreePlaces()
         {
             return Settings.MaxSlots-Parking._vechiles.Count;
         }

         public ReadOnlyCollection<Vehicle> GetVehicles()
         {
             if (Parking._vechiles.Count > 0)
             {
                 ReadOnlyCollection<Vehicle> read = new ReadOnlyCollection<Vehicle>(Parking._vechiles);
                 return read;
             }
             else
             {
                 Console.WriteLine();
                 return null;
             }
         }

         public void AddVehicle(Vehicle vehicle,decimal sum)
         {
             Parking._vechiles.Add(vehicle);
             TimerService LogTiemer = new TimerService(Settings.LogWritePediod,vehicle);
             TimerService PayTimer = new TimerService(Settings.PaymentPeriod,vehicle);
             _startedServices.Add(LogTiemer);
             _startedServices.Add(PayTimer);
             LogTiemer.Start();
             PayTimer.Start();
             LogService lg = new LogService(vehicle);
             _createdServices.Add(lg);
             Transaction AddingTransaction = new Transaction(vehicle,vehicle._type,sum,"Добавление на парковку");
             AddingTransaction.AddTransaction(lg);
             LogTiemer.Elapsed += new ElapsedEventHandler(LogRefresh);
             PayTimer.Elapsed += new ElapsedEventHandler(PayRefresh);
                 void LogRefresh(object s, ElapsedEventArgs e)
                 {
                     foreach (TransactionInfo t in lg._infos)
                     {
                         lg.Write(t.ToString());
                         lg._infos.Remove(t);
                     }
                    
                 }
                 void PayRefresh(object s, ElapsedEventArgs e)
                 {
                     if (vehicle._balance >= (decimal) Settings.GetTarifCoef(vehicle._type))
                     {
                         Transaction PayForTimerTransaction =
                             new Transaction(vehicle, (decimal) Settings.GetTarifCoef(vehicle._type), "Оплата по времени",1);
                         PayForTimerTransaction.AddTransaction(lg);
                     }
                     else
                     {
                         Transaction PayForTimerTransaction =
                             new Transaction(vehicle, (decimal) Settings.GetTarifCoef(vehicle._type)-((decimal) Settings.GetTarifCoef(vehicle._type)-vehicle._balance)+((decimal) Settings.GetTarifCoef(vehicle._type)-vehicle._balance)*(decimal)Settings.BillCoef, "Оплата по времени со штрафом",1);
                         PayForTimerTransaction.AddTransaction(lg);
                         Console.WriteLine("Не достаточно средст на счету вашей машины "+vehicle._id+"оплата стоянки начисляеться со штрафом");
                     }
                 }
         }
       
         public void RemoveVehicle(string vehicleId)
         {
             foreach (Vehicle v in Parking._vechiles)
             {
                 if (v._id == vehicleId)
                 {
                     Parking._vechiles.Remove(v);
                     Console.WriteLine("Снато с парковки");
                     break;
                 }
             }

             foreach (TimerService t in _startedServices)
             {
                 if (t._Car._id == vehicleId)
                 {
                     t.Stop();
                    ((ITimerService)t).Dispose();
                 }
                 
             }
             foreach (LogService l in _createdServices)
             {
                 if (l.Vehicle._id == vehicleId)
                 {
                     //((IDisposable)l).Dispose();
                 }
                 
             }
         }

         public void TopUpVehicle(Vehicle vehicle, decimal sum)
         {
             foreach (Vehicle v in Parking._vechiles)
             {
                 if (v._id == vehicle._id)
                 {
                     LogService lg=null;
                     foreach (LogService l in _createdServices)
                     {
                         if (l.Vehicle._id == vehicle._id)
                         {
                             lg = l;
                         }
                 
                     }
                     if (Consumer._balance >= (decimal) sum)
                     {
                         Transaction PayForUpTransaction =new Transaction(vehicle,sum, "Пополнение счета автомобиля");
                         PayForUpTransaction.AddTransaction(lg);
                     }
                     else
                     {
                         
                         Console.WriteLine("Не достаточно средст на счету ");
                     }
                     
                   
                 }
             }
         }

         public Collection<TransactionInfo> GetLastParkingTransactions(Vehicle car)
         {
             foreach (LogService s in _createdServices)
             {
                 if (s.Vehicle == car)
                 {
                     return s._infos;
                 }
                 
             }
                Console.WriteLine();
             return null;
         }

         public string ReadFromLog(Vehicle car)
         {
             foreach (LogService s in _createdServices)
             {
                 if (s.Vehicle == car)
                 {
                     return s.Read();
                 }
                 
             }

             return null;
         }
     }
 }