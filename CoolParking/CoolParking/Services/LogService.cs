﻿﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.

 using System;
 using System.Collections.ObjectModel;
 using System.IO;
 using System.Reflection;
 using CoolParking.Interfaces;
 using CoolParking.Models;

 namespace CoolParking.Services
 {
     public class LogService:ILogService
     {
         public string LogPath { get; } = "Transaction.txt";
         public  Collection<TransactionInfo> _infos=new Collection<TransactionInfo>();
         private string _vehicleId;
         public Vehicle Vehicle;
         public LogService(Vehicle v)
         {
             Vehicle = v;
             _vehicleId = v._id;
         }
         public void Write(string logInfo)
         {
            
             if (File.Exists(_vehicleId+LogPath))
             {
                 using (FileStream fileStream = new FileStream(_vehicleId+LogPath, FileMode.Open))
                 {
                     using (StreamWriter log = new StreamWriter(fileStream))
                     {
                         log.WriteLine(logInfo);
                     }
                 }
              
               
             }
             else
             {
                 
                 using (FileStream fileStream = new FileStream(_vehicleId+LogPath, FileMode.OpenOrCreate))
                 {
                     using (StreamWriter log = new StreamWriter(fileStream))
                     {
                         log.WriteLine(logInfo);
                     }
                 }
             }
         }
         
         public string Read()
         {
             if (File.Exists(_vehicleId+LogPath))
             {
                 using (FileStream fileStream = new FileStream(_vehicleId+LogPath, FileMode.Open))
                 {
                     using (StreamReader log = new StreamReader(fileStream))
                     {
                         return log.ReadToEnd();
                     }
                 }
                
               
             }
             else
             {
                 throw new InvalidOperationException();
             }
         }
         
         public  void AddTransaction(TransactionInfo info)
         {
             _infos.Add(info);
         }
     }
 }