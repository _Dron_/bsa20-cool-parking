﻿
 using System;

 namespace CoolParking.Models
 {
     public class TransactionInfo
     {
         protected Vehicle _vehicle;
         protected VehicleType _type;
         protected decimal _sum;
         protected string _paymentType;
         public TransactionInfo(Vehicle v,VehicleType type,decimal sum,string paymentType) 
         {
             _vehicle = v;
             _type = type;
             _sum = sum;
             _paymentType = paymentType;
         }

         public override string ToString()
         {
             return "Транзакция" + '\n' + _vehicle._id + '\n' + _type + '\n' + _sum.ToString() + '\n' + _paymentType;
         }
     }
 }