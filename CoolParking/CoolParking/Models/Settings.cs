﻿
 namespace CoolParking.Models
 {
     static class Settings
     {
         public static decimal StartParkingBalance = 0;
         public static int MaxSlots = 10;
         public static int PaymentPeriod=5000;
         public static int LogWritePediod=60000;
         public static double BillCoef=2.5;

         public static double GetTarifCoef(VehicleType type)
         {
             if (type == VehicleType.Bus)
             {
                 return 3.5f;
             }
             else if (type == VehicleType.Passenger)
             {
                 return 2;
             }
             else if (type == VehicleType.Truck)
             {
                 return 5;
             }
             else
             {
                 return 1;
             }
         }
     }
 }
