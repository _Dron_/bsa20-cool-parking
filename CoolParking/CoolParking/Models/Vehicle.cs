﻿

 using System;

 namespace CoolParking.Models
 {
     public class Vehicle
     {
         public  string _id { get; }
         public  readonly VehicleType _type;
         public decimal _balance = 0;
         public Vehicle(string id,  VehicleType type)
         {
             _id = id;
             
             _type = type;
         }

         public static string GenerateRandomRegistrationPlateNumber()
         {
             string[] cities = 
             {
                 "AA", "AB", "BH", "CA", "BK"
             };
             string[] random =
             {
                 "СН", "PO", "TE"
             };
             Random rand = new Random();
             return (cities[rand.Next(0, 4)] +"-" +rand.Next(0, 9999).ToString() + "-"+random[rand.Next(0, 2)]);
         }
     }
 }
