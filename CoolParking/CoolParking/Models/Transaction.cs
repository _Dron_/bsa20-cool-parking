﻿using System;
using CoolParking.Services;

namespace CoolParking.Models
{
    public class Transaction
    {
        protected Vehicle _vehicle;
        protected VehicleType _type;
        protected decimal _sum;
        protected string _paymentType;
        public Transaction(Vehicle v,VehicleType type,decimal sum,string paymentType)
        {
            _vehicle = v;
            _type = type;
            _sum = sum;
            _paymentType = paymentType;
            if (Consumer._balance >= sum)
            {
                Consumer._balance -= sum;
                v._balance += sum;
            }
           
        }
        public Transaction(Vehicle v,decimal sum,string paymentType,int i)
        {
            _vehicle = v;
            
            _sum = sum;
            _paymentType = paymentType;
            if (v._balance >= sum)
            {
                v._balance -= sum;
                Parking._balance += sum;
            }
           
        }
        public Transaction(Vehicle v,decimal sum,string paymentType)
        {
            _vehicle =v;
            _sum = sum;
            _paymentType = paymentType;
            if (Consumer._balance >= sum)
            {
                Consumer._balance -= sum;
                v._balance += sum;
            }
           
        }
        public void AddTransaction(LogService s)
        { 
            TransactionInfo info = new TransactionInfo(_vehicle,_type,_sum,_paymentType);
            s.AddTransaction(info);
        }
    }
}