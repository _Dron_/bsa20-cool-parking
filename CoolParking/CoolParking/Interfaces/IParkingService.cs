﻿﻿using System;
using System.Collections.ObjectModel;
 using CoolParking.Models;

 namespace CoolParking.Interfaces
{
    public interface IParkingService : IDisposable
    {
        decimal GetBalance();
        int GetCapacity();
        int GetFreePlaces();
        ReadOnlyCollection<Vehicle> GetVehicles();
        void AddVehicle(Vehicle vehicle,decimal sum);
        void RemoveVehicle(string vehicleId);
        void TopUpVehicle(Vehicle vehicle, decimal sum);
        Collection<TransactionInfo> GetLastParkingTransactions(Vehicle car);
        string ReadFromLog(Vehicle car);
    }
}
