﻿﻿using System;
using System.Collections.Generic;
using System.Text;
 using CoolParking.Models;

 namespace CoolParking
{
    public static class Menu
    {
        
        public static void MainMenu()
        {
       
            ConsoleKeyInfo key;
            do
            {
                Console.Clear();
                ShowMainMenu();
                key = Console.ReadKey();
                if (!Char.IsDigit(key.KeyChar))
                    Console.WriteLine("\nНеобходимо нажимать только цифры!");
                else
                {
                    Console.Clear();
                    switch (key.KeyChar)
                    {
                        case '1': AddCarToParking(); break;
                        case '2': DeleteCarFromParking(); break;
                        case '3': ReplenishmentAutoBalance(); break;
                        case '4': ShowTransactionStory(); break;
                        case '5': ShowParkingIncome(); break;
                        case '6': ShowFreePlaces(); break;
                        case '7': ShowLogFile(); break;
                    }
                    Console.WriteLine("\nДля выхода в меню нажмите на любую клавишу!");
                    Console.ReadKey();
                }
            } while (key.KeyChar != '0');
        }

        private static void ShowMainMenu()
        {
            Console.Write("\nМеню:\n1) Добавить авто на парковку\n" +
                "2) Удалить авто с парковки\n" +
                "3) Пополнить баланс авто\n" +
                "4) Просмотр истории транзакций за последнюю минуту\n" +
                "5) Просмотр общего дохода парковки\n" +
                "6) Просмотр свободных мест\n" +
                "7) Просмотр лог файла\n"+
                "0) Выход из программы\nПросто нажмите нужную цифру: ");
        }

        private static void AddCarToParking()
        {
            try
            {
                Console.WriteLine("Вы зашли в режим добавления авто!\n");
                if (Settings.MaxSlots - Parking._vechiles.Count == 0)
                {
                    Console.WriteLine("Вы не можете припоркавать автомобиль! Все места на паркове заняты!");
                    return;
                }

                try
                {

                    string carNumber = Vehicle.GenerateRandomRegistrationPlateNumber();
                    Console.Write("Введите баланс который будет изначально на авто: ");
                    decimal carBalance = decimal.Parse(Console.ReadLine());
                    int carType;
                    do
                    {
                        Console.Write(
                            "Выберите тип автомобиля:\n\t1)Passenger\n\t2)Truck\n\t3)Bus\n\t4)Motorcycle\nВведите нужное число: ");
                        carType = Convert.ToInt32(Console.ReadLine());
                    } while (carType < 0 || carType > 4);

                    VehicleType type;
                    if (carType == 1)
                    {
                        type = VehicleType.Passenger;
                    }
                    else if (carType == 2)
                    {
                        type = VehicleType.Truck;
                    }
                    else if (carType == 3)
                    {
                        type = VehicleType.Bus;
                    }
                    else
                    {
                        type = VehicleType.Motorcycle;
                    }

                    Vehicle car = new Vehicle(carNumber, type);
                    Parking._parking.AddVehicle(car, carBalance);

                }
                catch (FormatException)
                {
                    Console.WriteLine("Ввод некорректный! Баланс и тип авто необходимо вводить числами!");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            catch (Exception ex)
            {
                MainMenu();
            }
        }

        private static void DeleteCarFromParking()
        {
            try{
            Console.Write("Вы вошли в режим удаления авто!");
            if (Parking._vechiles.Count == 0)
                Console.WriteLine("\nАвтомобилей в паркинге нет!");
            else
            {
                A:
                int ReadKey;
                int i = 1;
                foreach (Vehicle v in Parking._vechiles)
                {
                    Console.WriteLine("Выберете вашу машину");
                    Console.WriteLine(i+") "+v._id);
                    i++;
                }

                ReadKey = Convert.ToInt32(Console.ReadLine());
                try
                {
                  Parking._parking.RemoveVehicle(Parking._vechiles[ReadKey-1]._id);
                }
                catch(Exception ex)
                {
                    Console.WriteLine("Ошибка");
                    goto A;
                }

                ShowMainMenu();
            }
        }
        catch (Exception ex)
        {
            MainMenu();
        }
        }

        private static void ReplenishmentAutoBalance()
        {
            try{
            Console.WriteLine("Вы вошли в режим пополнения баланса авто!");
            if (Parking._vechiles.Count == 0)
                Console.WriteLine("\nАвтомобилей в паркинге нет!");
            else
            {
                A:
                int ReadKey;
                int i = 1;
                foreach (Vehicle v in Parking._vechiles)
                {
                    Console.WriteLine("Выберете вашу машину");
                    Console.WriteLine(i+") "+v._id);
                    i++;
                }

                ReadKey = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Теперь введите суму");
                double charge = double.Parse(Console.ReadLine());
             
                try
                {
                    if (charge < 0)
                        Console.WriteLine("Вы не можете уменьшить баланс!");
                    else
                    {

                        Parking._parking.TopUpVehicle(Parking._vechiles[ReadKey-1],(decimal)charge);
                        Console.WriteLine($"Баланс после пополнения = {  Parking._vechiles[ReadKey-1]._balance}.");
                    }
                }
                catch(Exception ex)
                {
                    Console.WriteLine("Ошибка");
                    goto A;
                }
               
            }
            }
            catch (Exception ex)
            {
                MainMenu();
            }
        }
        private static void ShowTransactionStory()
        {
            try{
            int ReadKey;
            int i = 1;
            foreach (Vehicle v in Parking._vechiles)
            {
                Console.WriteLine("Выберете вашу машину");
                Console.WriteLine(i+") "+v._id);
                i++;
            }

            ReadKey = Convert.ToInt32(Console.ReadLine());
            foreach (TransactionInfo tr in Parking._parking.GetLastParkingTransactions(Parking._vechiles[ReadKey - 1]))
            {
                Console.WriteLine();
                Console.WriteLine(tr.ToString());
            }
            }
            catch (Exception ex)
            {
                MainMenu();
            }
        }

        private static void ShowParkingIncome()
        {
            Console.WriteLine($"\nДоход паркига составляет: {Parking._balance}");
        }

        private static void ShowFreePlaces()
        {
            Console.WriteLine($"\nКоличество занятых мест: {Parking._vechiles.Count}");
            Console.WriteLine($"Количество свободных мест: {Settings.MaxSlots-Parking._vechiles.Count}");
        }

        private static void ShowLogFile()
        {
            try{
            int ReadKey;
            int i = 1;
            foreach (Vehicle v in Parking._vechiles)
            {
                Console.WriteLine("Выберете вашу машину");
                Console.WriteLine(i+") "+v._id);
                i++;
            }

            ReadKey = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(Parking._parking.ReadFromLog(Parking._vechiles[ReadKey - 1]).ToString());
            }
            catch (Exception ex)
            {
                MainMenu();
            }
        }
    }
}
